# Speech Emotion Recognition

This is a project delivered for the "Data Science Lab" course of the "Data Science & Engineering" master's degree @ Politecnico di Torino on 2022-June.

## About

This project deals with the problem of speech emotion recognition for short snippets of audio.
We use the spectrograms to perform feature engineering for representing the data into a feature space.
Finally we perform classification via SVM. We show that the proposed approach gives satisfying results for the SER task

Students competed with each other on a public leaderboard. Their models were tested on a held-out set inaccessible to them. 

Our model achieved the following score on the leaderboard: f1_macro = 0.69
